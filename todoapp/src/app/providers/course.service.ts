import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Course } from '../models/course.model';


@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http: HttpClient) { }

  private coursesEndpoint: string = 'http://localhost:8081/api/courses';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getCourses() : Observable<Course[]> {
    return this.http.get(this.coursesEndpoint, this.httpOptions)
    .pipe(map(res => <Course[]>res));
    }

    getACourse(id: number) : Observable<any> {
      return this.http.get(this.coursesEndpoint + "/" + id,
      this.httpOptions);
      }
}
