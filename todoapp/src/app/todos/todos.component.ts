import { Component, OnInit } from '@angular/core';
import { Todos } from '../models/todos.model';
import { TodoService } from '../providers/todo.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit{
  mytodoarray: Array<Todos> = [];

constructor(private gettodo: TodoService){}

  ngOnInit() {
    
    this.gettodo.getTodos().subscribe(data => {
    this.mytodoarray = data;
    });
    }
}
