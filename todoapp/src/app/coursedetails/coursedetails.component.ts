import { Component, OnInit } from '@angular/core';
import { Course } from '../models/course.model';
import { CourseService } from '../providers/course.service';

@Component({
  selector: 'app-coursedetails',
  templateUrl: './coursedetails.component.html',
  styleUrls: ['./coursedetails.component.css']
})
export class CoursedetailsComponent implements OnInit {
  myCourse: Array<Course> = [];
  id: number = -1;



  constructor(private getACourse: CourseService) {

  }

  ngOnInit(): void {
    //loadDetails(urlParams);
    console.log("I am here");
    const urlParams = new URLSearchParams(location.search);
    
    if (urlParams.has("courseid") === true) {
      
      this.id = Number(urlParams.get("courseid"));
    }

    this.getACourse.getACourse(this.id).subscribe(data => {
      this.myCourse = data;
    });
  }
}
