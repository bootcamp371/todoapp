import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { TodoService } from './providers/todo.service';
import { AppComponent } from './app.component';
import { TodosComponent } from './todos/todos.component';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { UsersComponent } from './users/users.component';
import { UserService } from './providers/user.service';
import { CoursesComponent } from './courses/courses.component';
import { CoursedetailsComponent } from './coursedetails/coursedetails.component';


const appRoutes: Routes = [
  { path: '', component: HeaderComponent },
  { path: 'todos', component: TodosComponent },
  { path: 'users', component: UsersComponent },
  { path: 'courses', component: CoursesComponent},
  { path: 'coursedetails', component: CoursedetailsComponent}
  // { path: 'search', component: SearchComponent },
  // { path: '**', component: HomeComponent } // 404 goes home!

];

@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    HeaderComponent,
    UsersComponent,
    CoursesComponent,
    CoursedetailsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule

  ],
  providers: [TodoService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
