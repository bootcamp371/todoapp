import { Component, OnInit } from '@angular/core';
import { Course } from '../models/course.model';
import { CourseService } from '../providers/course.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  myCourses: Array<Course> = [];

  constructor(private getCourses: CourseService) { }

  ngOnInit(): void {
    this.getCourses.getCourses().subscribe(data => {
      this.myCourses = data;
      });
  }
}
